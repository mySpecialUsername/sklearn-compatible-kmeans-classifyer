"""KMeans Classification"""

# Author: Gor G.
# BSD 3-Clause License


import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.utils.multiclass import unique_labels

import warnings


class KMeansClassifier(ClassifierMixin, BaseEstimator):
    """Classifier inspired by the K-means clustering algorithm.
    Short description :
    During the fit, it calculates the means of each class in the provided data, a vector of normalization for each class.
    During the estimation it calulates the vector diff_c = sample-mean_class_x for each given class, than normalises the diff_c vector, to finally calculate the norm of the said diff_c vector, and selects the class for wich the norm of the differce vecor is the smallest.

    Parameters
    ----------
    p : int, default=2
        Power parameter for the Minkowski metric. When p = 1, this is
        equivalent to using manhattan_distance (l1), and euclidean_distance
        (l2) for p = 2. For arbitrary p, minkowski_distance (l_p) is used.

    constant_feature_replacement_factor : float, default=1
        See the description of the std_invs_ attribute.

    zero_std_warning : bool, default = True
        turns on and off the warning when replacing a zero STD by a non zero value.

    normalization_type : {'none', 'std_per_axe', 'std_mean'} or a dictionary, default='std_per_axe'
        It defines the way the vecor of difference between the sample and the centroid(the mean) of a class is normalised. ( For instance if we're trying to figure out whether a quadripede is a cat or a tigere, and we have a feature corresponding to the average colors of a given simple, the standard deviation of the cats for the color feature will be much biger than the std of the tiges, hence when we have an unusually colored simple, it is most likely to be a cat than a tigre, and to take that in to account we may want to reduce the importance of the color feature in the calculation of the distance from the cats class by dividing the average_color_of_the_cats - color_of_a_given_sample by the standart deviation of the class cat feature color, and corresponding increse the importance of the feature color by dividing the corrsponding differce by the STD of the class tigre feature color)
        Possible values:
        - 'none' : the diff_c vector isn't modifyed, to determine the distance we directly calculate it's norm
        - 'std_per_axe' : normalises the diff_c vector by dividing each of it's coordinate by the corresponding coordinate in the vector
        - 'std_mean' : normalises the diff_c vector by dividing it by norm of the standart deviation vector of the given class. (This is used when it is empiriclay known that all the features are supposed to have the same standard deviation, for exemple when we're dealing with the distribution of pizza in a city, and we want to figure out who sells pizza in wich house, there is no special reason for a pizza seller to have a more scattered clientel along the north-south axe than along the east-west axe.)
        - [dictionary] : a user-defined dictionary {class : weight_vector} the weight_vector's  coefficients are applied on each coefficient of the diff vector




    Attributes
    ----------
    classes_ : array of shape (n_classes_)
        Class labels known to the classifier
    n_features_in_ : int
        Number of features seen during :term:`fit`.
    n_classes_ : int
        Number of classes seen during :term:`fit`.
    n_samples_fit_ : int
        Number of samples in the fitted data.
    means_ : array of shape (n_samples_fit_, n_classes_)
        Each row is the mean vector of a class.
    std_invs_ : array of shape (n_samples_fit_, n_classes_)
        Determines de weights of each feature of each class in the calculation of the distance from the mean of a class.
        std_invs_ = 1/Stds.  Where Stds in the perfect case is an array of shape (n_samples_fit_, n_classes_) in which each row represents the standart deviation of the featuers for the samples of the fitted data of a class.
        If they exist, the STD values in the Stds array that are equal to zero are replaced by a proportion(defined by constant_feature_replacement_factor) of the smallest non zero STD of that feature from the other classes. If the STD of that featuer is equal to zero for all the classes, than the STD values of that feature are replaced by the value of the smallest non zero STD of all classes and featuers. If for all classes and features the STD is zero, than the weights of all classes and fatures are set to 1.



    ### WARNING description will be added soon

    Examples
    --------
    see the demonstaration file https://gitlab.com/main198/sklearn-compatible-kmeans-classifyer/-/blob/main/kmeans_classifyer_demonstration.py
    """



    def __init__(self, p = 1, constant_feature_replacement_factor = 1, normalization_type = 'std_per_axe', zero_std_warning=True):
        self.p = p
        self.constant_feature_replacement_factor = constant_feature_replacement_factor
        self.zero_std_warning = zero_std_warning
        self.normalization_type = normalization_type

    def dist(self, x):
        return np.linalg.norm(x, ord = self.p)




    def _fit_std_per_axe(self, X, y):

        Means = []
        Stds = []
        for c in self.classes_:
            C = X[y==c]
            Means.append(np.mean(C, axis = 0))
            std_c = np.std(C, axis = 0)
            Stds.append(std_c)

        Stds = np.array(Stds)

        # handeling featuers that are consant for some classes (std=0)
        if np.any(Stds==0):
            nonzero_stds = Stds[np.where(Stds!=0)]
            if nonzero_stds.size:
                min_nonzero_std = np.min(nonzero_stds)
                for feature in range(self.n_features_in_):
                    zero_indexs = np.where(Stds[:, feature]==0)
                    if 0<zero_indexs[0].size<self.n_classes_:
                        Stds[zero_indexs, feature] = np.min(Stds[np.where(Stds[:, feature]!=0), feature ]) * self.constant_feature_replacement_factor
                    else:
                        Stds[zero_indexs, feature] = min_nonzero_std* self.constant_feature_replacement_factor
                if self.zero_std_warning:
                    warnings.warn('zero STD substitution warning. (To turn off this warning set the attribute zero_std_warning to False.)\n    In the given data set, some of the features of some classes are constant (STD = 0), hence to prevent divisions by zero and the creation of infite weights the STD values of these featuers will be replaced by a proportion of the smallest non-zero STD for that featuer : see documentation PDF(documentation is not yet ready)', RuntimeWarning, stacklevel=2) # WARNING there is no documentation yet. (remove the "(documentation is not yet ready)" )
                Std_invs = 1/Stds
            else:
                if self.zero_std_warning:
                    warnings.warn("zero STD substitution warning. (To turn off this warning set the attribute zero_std_warning to False.) \n in the provided data set all featuers for all classes are constant, the weights of all fatures of all classes will be set to 1.", RuntimeWarning, stacklevel=2)
                Std_invs = np.ones(Stds.shape)
        else:
            Std_invs = 1/Stds

        return Means, Std_invs


    def _fit_none(self, X, y):
        Means = []
        for c in self.classes_:
            C = X[y==c]
            Means.append(np.mean(C, axis = 0))
        Std_invs = np.ones((self.n_classes_, self.n_features_in_))
        return Means, Std_invs


    def _fit_std_mean(self, X, y):
        Means = []
        Stds = []
        for c in self.classes_:
            C = X[y==c]
            Means.append(np.mean(C, axis = 0))
            std_c = np.std(C, axis = 0)
            Stds.append([self.dist(std_c)])
        Stds = np.array(Stds)

        if np.any(Stds==0):
            if np.any(Stds!=0):
                Stds[np.where(Stds==0)] = np.min(Stds[np.where(Stds!=0)])
                if self.zero_std_warning:
                    warnings.warn('zero STD substitution warning. (To turn off this warning set the attribute zero_std_warning to False.)\n    In the given data set, some classes are constant (STD = 0), hence to prevent divisions by zero and the creation of infite weights the STD values of these featuers will be replaced by a proportion of the smallest non-zero STD for that class : see documentation PDF(documentation is not yet ready)', RuntimeWarning, stacklevel=2) # WARNING there is no documentation yet. (remove the "(documentation is not yet ready)" )
                Std_invs = 1/Stds
            else:
                if self.zero_std_warning:
                    warnings.warn("zero STD substitution warning. (To turn off this warning set the attribute zero_std_warning to False.) \n in the provided data set all featuers for all classes are constant, the weights of all classes will be set to 1.", RuntimeWarning, stacklevel=2)
                Std_invs = np.ones((self.n_classes_, 1))
        else:
            Std_invs = 1/Stds

        return Means, Std_invs


    def fit(self, X, y):
        """Fitting function for a classifier.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            The training input samples.
        y : array-like, shape (n_samples,)
            The target values. An array of int.

        Returns
        -------
        self : object
            Returns self.
        """

        # Check that X and y have correct shape
        X, y = check_X_y(X, y, ensure_min_samples=2)
        # Store the classes seen during fit
        self.classes_ = unique_labels(y)

        self.X_ = X
        self.y_ = y

        self.n_features_in_ = X[0].size
        self.n_classes_ = self.classes_.size


        if self.normalization_type== 'std_per_axe':
            Means, Std_invs = self._fit_std_per_axe( X, y)
        elif self.normalization_type== 'none':
            Means, Std_invs = self._fit_none( X, y)
        elif self.normalization_type== 'std_mean':
            Means, Std_invs = self._fit_std_mean( X, y)
        else:
            raise ValueError(" %s is not a valid value for normalization_type. Valide values : {'none', 'std_per_axe', 'std_mean'}"%(str(self.normalization_type)))




        self.means_ = Means
        self.std_invs_ = Std_invs

        # Return the classifier
        return self

    def predict_std_mean(self, X):
        closest = []
        dists_from_classes = np.empty((self.n_classes_), dtype="float32")
        for x  in X:
            for i in range(self.n_classes_):
                diff_c = self.means_[i] - x
                dists_from_classes[i] = self.dist(diff_c)*self.std_invs_[i]
            closest.append(np.argmin(dists_from_classes))

        return closest

    def predict_std_per_axe_or_none(self, X):
        closest = []
        dists_from_classes = np.empty((self.n_classes_), dtype="float32")
        for x  in X:
            for i in range(self.n_classes_):
                diff_c = self.means_[i] - x
                dists_from_classes[i] = self.dist(diff_c*self.std_invs_[i])
            closest.append(np.argmin(dists_from_classes))

        return closest

    def predict(self, X):
        """ Prediction for a classifier.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            The input samples.

        Returns
        -------
        y : ndarray, shape (n_samples,)
            The label for each sample is the label of the closest sample
            seen during fit.
        """
        # Check is fit had been called
        check_is_fitted(self, ['X_', 'y_'])

        # Input validation
        X = check_array(X)
        if X.shape[1]!=self.n_features_in_:
            raise ValueError("the number of featuers of the data to predict didn't match with the number of featuers of the fitted data. \n Fitted data features : %d \n Prediction data features : %d"%(self.n_features_in_, X.shape[1] ))

        if self.normalization_type in ['std_per_axe','none']:
            closest = self.predict_std_per_axe_or_none(X)
        else:
            # at this point it hase been varifyed in the fit that normalization_type is one of the valid values, so there is no need of checking that it's 'std_mean'
            closest = self.predict_std_mean(X)

        return self.classes_[closest]

    def score(self, X, y):
        check_is_fitted(self, ['X_', 'y_'])
        return np.count_nonzero(y==self.predict(X))/y.size
