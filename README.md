# sklearn compatible KMeans classifier


## Classifier inspired by the K-means clustering algorithm.

When `normalization_type` is `'none'`:

- During the `fit`, it calculates the means of each class in the provided data.

- During the `predict`it calculates the norm of the vector `diff_c = sample-mean_class_c` for each given class and returns the class for wich `norm(diff_c)` is minimal.


When `normalization_type` is `'std_mean'`:

- During the `fit`, it calculates the means of each class in the provided data, and a _scalar_ of normalization for each class contained in `std_invs` wich is a matrix of shape `(n_classes, 1)`. `std_invs[c] = 1/Stds[c]` where `Stds[c]` is the norm of the vector that is composed from the standart deviations of all feature in the class `c`.

- During the `predict`it calculates the norm of the vector `diff_c = sample-mean_class_c` for each given class, and returns the class for wich `diff_c * std_invs[c]` is minimal. 


When `normalization_type` is `'std_per_axe'`:

- During the `fit`, it calculates the means of each class in the provided data, and a _vector_ of normalization for each class contained in the matrix  `std_invs` shape `(n_classes, n_features_in_)`. `std_invs[c, f] = 1/Stds[c, f]` where `Stds[c, f]` is the standart deviation of the feature `f` in the class `c`. 

- - In the case when the `Stds[c, f]` is zero, `Stds[c, f]` is replaced by the value of the smallest non zero std of the feature `f` across all the classes, multiplyed by `constant_feature_replacement_factor`(usually `<=1`). If  all the values of the feature `f` acorss all the classes are zero, than the smallest non zero value of all classes is used. If all the features of all classes are constant, and consequently all the values of the `Stds` matrix are zero, `std_invs`is a matrix containing only ones, hence no normalization is takng place.

- During the `predict` it calulates the vector `diff_c = sample-mean_class_c` for each given class, than normalises the `diff_c` by doing `diff_c[f] = diff_c[f] * std_invs[c,f]` for all features `f`, to finally calculate the norm of the normalised `diff_c` vector, and selects the class for wich the norm of the vecor `diff_c` is the smallest.

## WARNING this project is still in progess, some functions of sklearn may not work as expected. 
### Tested functionalities are: 

`fit` and `predict` are tested.

`score` is implemented.

`cross_val_score` tested with the deafault scoring and `'f1_macro'` scoring.
