import numpy as np
from sklearn.model_selection import cross_val_score
from sklearn.datasets import load_iris
from sklearn.datasets import load_digits

from ClassifierKMeans import KMeansClassifier as kmeans



clf_means = kmeans(zero_std_warning=False)


irisData = load_iris()
X=irisData.data
Y=irisData.target

sucess =cross_val_score(clf_means, X, Y, cv = 5)
f1 = cross_val_score(clf_means, X, Y, cv = 10, scoring='f1_macro')
print("Cross Val Score sucess rate with KMeans : \n %f (+/-) %f\n F-score (global) : %f (+/-) %f\n "
    %(sucess.mean(), sucess.std(), f1.mean(), f1.std()) )


# output :
#>>>Cross Val Score sucess rate with KMeans :
#>>> 0.940000 (+/-) 0.044222
#>>> F-score (global) : 0.931953 (+/-) 0.053097




digitsData = load_digits()
X=digitsData.data
Y=digitsData.target

sucess =cross_val_score(clf_means, X, Y, cv = 5)
f1 = cross_val_score(clf_means, X, Y, cv = 10, scoring='f1_macro')
print("Cross Val Score sucess rate with KMeans : \n %f (+/-) %f\n F-score (global) : %f (+/-) %f\n "
    %(sucess.mean(), sucess.std(), f1.mean(), f1.std()) )


#>>>Cross Val Score sucess rate with KMeans :
#>>> 0.863680 (+/-) 0.043413
#>>> F-score (global) : 0.869013 (+/-) 0.056628




X = np.empty((1000, 4), dtype="int8")
Y = np.random.randint(0,3,1000)

X[np.where(Y==0)]=1
X[np.where(Y==1)]=2
X[np.where(Y==2)]=3

sucess =cross_val_score(clf_means, X, Y, cv = 5)
f1 = cross_val_score(clf_means, X, Y, cv = 10, scoring='f1_macro')
print("Cross Val Score sucess rate with KMeans : \n %f (+/-) %f\n F-score (global) : %f (+/-) %f\n "
    %(sucess.mean(), sucess.std(), f1.mean(), f1.std()) )


#>>>Cross Val Score sucess rate with KMeans : 
#>>> 1.000000 (+/-) 0.000000
#>>> F-score (global) : 1.000000 (+/-) 0.000000
 

